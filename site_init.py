# -*- coding: utf-8 -*-

#
# Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the
# sale, use or other dealings in this Software without prior written
# authorization.


"""This module implements a SConsEnvironment subclass (see DEnv) providing useful
D and installation related functionality.
"""

from os import unlink
from os.path import join, isabs
import atexit
import sys

from SCons.Script.SConscript import SConsEnvironment
from SCons.Script import *
import SCons.Tool

class ArchMap(dict):

    def __init__(self, *args, **kwargs):
        super(ArchMap, self).__init__(*args, **kwargs)

    def __getitem__(self, key):
        for k in self.keys():
            if key.find(k) != -1:
                return dict.__getitem__(self, k)

    def __repr__(self):
        dictrepr = dict.__repr__(self)
        return '%s(%s)' % (type(self).__name__, dictrepr)

    def get(self, k, default):
        res = self.__getitem__(k)
        if res != None:
            return res
        return default


def DMDParseConfigCallBack(env, pkg_config):
    args = pkg_config.split()
    for arg in args:
        if arg.startswith('-L') or arg.startswith('-defaultlib'):
            env.Append(DLINKFLAGS=arg)
        elif arg.startswith('-I'):
            env.Append(DPATH=arg[2:])
        else:
            env.Append(DFLAGS=arg)


def DPkgConfig(env, source):
    env.ParseConfig(source, DMDParseConfigCallBack)


def CheckPKGConfig(context, version):
    context.Message('Checking for pkg-config... ')
    ret = context.TryAction(
        'pkg-config --atleast-pkgconfig-version=%s' % version)[0]
    if not ret:
        context.Message('%s not found, check config.log for details' % (version))
    context.Result(ret)
    return ret


def CheckPKG(context, name):
    context.Message('Checking for %s... ' % name)
    ret = context.TryAction('pkg-config --exists \'%s\'' % name)[0]
    if not ret:

        context.Message('%s not found, check config.log for details' % (name))
    context.Result(ret)
    return ret


class DEnv(SConsEnvironment):

    def __init__(self, platform=None, tools=None, toolpath=None, variables=None, parse_flags=None, **kwargs):
        if tools is None:
            patched_tools = ['default', 'gettext', 'dhelpers']
        else:
            patched_tools = set(['default', 'gettext', 'dhelpers']) | set(tools)
        super(DEnv, self).__init__(platform, patched_tools, toolpath,
                                   variables, parse_flags, SHOBJSUFFIX='.o', **kwargs)

        self.DistClean()
        SCONS_VARS_FILE = ".scons.vars.conf"
        opts = kwargs.pop('opts', Variables(SCONS_VARS_FILE))
        if kwargs.has_key('PACKAGE_NAME'):
            package_name = kwargs.pop('PACKAGE_NAME')
            self['PACKAGE_NAME'] = package_name
        else:
            if self.has_key('PACKAGE_NAME'):
                package_name = self['PACKAGE_NAME']
            else:
                package_name = 'DEFAULT_PACKAGE_NAME'
        opts.Add(PathVariable('DESTDIR', 'Installation root',
                              '', PathVariable.PathAccept))
        opts.Add(PathVariable('prefix', 'A prefix used in constructing the default values of the variables listed below',
                              '/usr/local', PathVariable.PathAccept))
        opts.Add(PathVariable('exec_prefix', 'A prefix used in constructing the default values of some of the variables listed below',
                              '${prefix}', PathVariable.PathAccept))
        opts.Add(PathVariable('bindir', 'The directory for installing executable programs that users can run',
                              '${exec_prefix}/bin', PathVariable.PathAccept))
        opts.Add(PathVariable('sbindir', 'The directory for installing executable programs that can be run from the shell, but are only generally useful to system administrators',
                              '${exec_prefix}/sbin', PathVariable.PathAccept))
        opts.Add(PathVariable('libexecdir', 'The directory for installing executable programs to be run by other programs rather than by users',
                              '${exec_prefix}/libexec', PathVariable.PathAccept))
        opts.Add(PathVariable('libdir', 'A prefix used in constructing the default values of the variables listed below',
                              '${exec_prefix}/lib', PathVariable.PathAccept))
        opts.Add(PathVariable('includedir', 'A prefix used in constructing the default values of the variables listed below',
                              '${prefix}/include', PathVariable.PathAccept))
        opts.Add(PathVariable('datarootdir', 'A prefix used in constructing the default values of the variables listed below',
                              '${prefix}/share', PathVariable.PathAccept))
        opts.Add(PathVariable('datadir', 'A prefix used in constructing the default values of the variables listed below',
                              '${datarootdir}', PathVariable.PathAccept))
        opts.Add(PathVariable('sysconfdir', 'A prefix used in constructing the default values of the variables listed below',
                              '${prefix}/etc', PathVariable.PathAccept))
        opts.Add(PathVariable('localedir', 'A prefix used in constructing the default values of the variables listed below',
                              '${datarootdir}/locale', PathVariable.PathAccept))
        opts.Add(PathVariable('docdir', 'The directory for installing documentation files (other than Info) for this package',
                              '${datarootdir}/doc/${PACKAGE_NAME}', PathVariable.PathAccept))
        opts.Add(EnumVariable('build_type', 'debug/release',
                              'release', allowed_values=('debug', 'release')))
        opts.Add(EnumVariable('arch', 'Architecture to build for', 'x86_64' if sys.maxsize > 2147483647 else 'i386',
                              allowed_values=('x86_64', 'i386'), map=ArchMap({'x86_64': 'x86_64', 'i386': 'i386'})))
        opts.Update(self)
        Help(opts.GenerateHelpText(self), append=True)
        if self['build_type'] == 'release':
            self.Append(DFLAGS=['-O', '-release',
                                '-inline', '-boundscheck=off'])
            if sys.platform != 'win32':
                # These are GCC specific
                self.Append(DLINKFLAGS=['-L-zrelro', '-L-znow'])
        else:
            self.Append(DFLAGS=['-g', '-debug'])
            self.Append(DLINKFLAGS=[])

        if self['arch'] == 'i386':
            self.Append(DFLAGS=['-m32'])
            self.Append(DLINKFLAGS=['-m32'])
        else:
            self.Append(DFLAGS=['-m64'])
            self.Append(DLINKFLAGS=['-m64'])
        build_dir = self.subst('build/${arch}/${build_type}')
        self.DistClean(SCONS_VARS_FILE)
        self.DistClean('build')
        self['builddir'] = build_dir

        # Support for -J flags
        self['_DIMPFLAGS'] = '${_concat(DIMPPREFIX, DIMPPATH, DIMPSUFFIX, __env__, RDirs, TARGET, SOURCE)}'
        self['DIMPPREFIX'] = '-J'
        self['DIMPSUFFIX'] = ''

        # def isD(env, source):
        #     if not source:
        #         return 0
        #     for s in source:
        #         ext = os.path.splitext(str(s))[1] # 8< Start of patch >8
        #         if ext == '.d':
        #             return 1                       # 8< End of patch >8
        #         if s.sources:
        #             ext = os.path.splitext(str(s.sources[0]))[1]
        #             if ext == '.d':
        #                 return 1
        #     return 0

        # The raison d'être of these builders is twofold:
        # - generate better code by giving the compiler more code at a time so it has better optimisation opportunities (by compiling multiple source files at once)
        # - delegate the linking of programs/shared library and creation of libraries to DMD,
        #   which brings us a step closer to a unified DMD tool that works on Linux, Windows and MacOS...

        # DProgram builder that build multiple source (no object files generation)
        self['DLINKCOM'] = '$DLINK -of$TARGET $_DINCFLAGS $_DVERFLAGS $_DIMPFLAGS $_DDEBUGFLAGS $_DFLAGS $DLINKFLAGS $__DRPATH $SOURCES $_DLIBDIRFLAGS $_DLIBFLAGS'
        program = SCons.Builder.Builder(action=SCons.Action.Action("$DLINKCOM", "$DLINKCOMSTR"),
                                        emitter='$PROGEMITTER',
                                        prefix='$PROGPREFIX',
                                        src_suffix='$DFILESUFFIX',
                                        suffix='$PROGSUFFIX',
                                        target_scanner=ProgramScanner)
        self['BUILDERS']['DProgram'] = program

        # DSharedLibrary builder that build multiple source (no object files generation)
        self['SHDLINKCOM'] = '$DLINK -of$TARGET $_DINCFLAGS $_DVERFLAGS $_DIMPFLAGS $_DDEBUGFLAGS $_DFLAGS $DSHLINKFLAGS $__DSHLIBVERSIONFLAGS $__DRPATH $SOURCES $_DLIBDIRFLAGS $_DLIBFLAGS'
        action_list = [SCons.Defaults.ShLinkAction, SCons.Tool.LibSymlinksAction]
        shared_lib = SCons.Builder.Builder(action=action_list,
                                           emitter="$SHLIBEMITTER",
                                           prefix=SCons.Tool.ShLibPrefixGenerator,
                                           suffix=SCons.Tool.ShLibSuffixGenerator,
                                           target_scanner=ProgramScanner,
                                           src_suffix='$DFILESUFFIX')
        self['BUILDERS']['DSharedLibrary'] = shared_lib
        action_list = [SCons.Action.Action("$DLIBCOM", "$DLIBCOMSTR")]

        # DLibrary builder that build multiple source (no object files generation)
        self["DLIBCOM"] = '$DC -lib $_DLIBFLAGS $_DINCFLAGS $_DVERFLAGS $_DIMPFLAGS $_DDEBUGFLAGS $_DFLAGS -of$TARGET $SOURCES'
        static_lib = SCons.Builder.Builder(action=action_list,
                                           emitter='$LIBEMITTER',
                                           prefix='$LIBPREFIX',
                                           suffix='$LIBSUFFIX',
                                           src_suffix='$DFILESUFFIX')
        self['BUILDERS']['DStaticLibrary'] = static_lib
        # We're overriding the LINK variable here so that means this environment will not work with anything else than D code...
        # A workaround for this is to patch the SCons/Tools/DCommon.py file like so:
        self['LINK'] = '$DC'
        self['LINKCOM'] = self['DLINKCOM']
        self['SHLINKCOM'] = self['SHDLINKCOM']

        atexit.register(DEnv.clean_sconsign)

    def DistClean(self, targets=[]):
        """Add a "distclean" alias (to be used in conjunction with the '-c' command line argument!)
        that adds a few things to be cleaned.
        """
        DIST_CLEAN_TARGETS = [".sconf_temp", "config.log"]
        self.Clean("distclean", DIST_CLEAN_TARGETS)
        # Just accumulate the clean targets in the alias
        self.Clean("distclean", targets)
        self.Alias("distclean")

    def GNUInstall(self, target, source):
        """Wrap SCons's Install() method to optionally prepend ${DESTDIR} if it is set
        """
        if self['DESTDIR'] is not '':
            target = join(self['DESTDIR'], target)
        return self.Install(target, source)

    def GNUInstallAs(self, target, source):
        """Wrap SCons's InstallAs() method to optionally prepend ${DESTDIR} if it is set
        """
        if self['DESTDIR'] is not '':
            target = join(self['DESTDIR'], target[1:]
                          if isabs(target) else target)
        return self.InstallAs(target, source)

    @staticmethod
    def clean_sconsign():
        """Nuke .sconsign.dblite, effectively causing full forced rebuilds
        """
        if "distclean" in COMMAND_LINE_TARGETS and GetOption('clean'):
            try:
                unlink('.sconsign.dblite')
                print ("Removed .sconsign.dblite")
            except Exception, e:
                pass

    def Configure(self, *args, **kwargs):
        return super(DEnv, self).Configure(custom_tests={
            'CheckPKGConfig' : CheckPKGConfig,
            'CheckPKG' : CheckPKG},
                                           *args,
                                           **kwargs)
