#
# Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the
# sale, use or other dealings in this Software without prior written
# authorization.

from os.path import dirname, join, abspath
from os import chdir, getcwd, devnull, system
import subprocess
import json
from SCons.Script import *


class dub_describe(object):
    def __init__(self, dub_file, dub_cfg=None, dub_args=[]):
        self.dub_file = dub_file
        self.dub_cfg = dub_cfg
        self.dub_args = dub_args
        self.dir_stack = None

    def __enter__(self):
        self.dir_stack = getcwd()
        chdir(dirname(self.dub_file))
        dev_null = open(devnull, "w")
        cmd_line = ['dub', 'describe']
        if self.dub_cfg:
            cmd_line += ['-c', self.dub_cfg]
        cmd_line += self.dub_args
        return json.loads(subprocess.check_output(cmd_line, stderr=dev_null))

    def __exit__(self, type, value, traceback):
        chdir(self.dir_stack)

class DubDescription(object):
    def __init__(self, source, dub_cfg, dub_args):
        self.dub_source = source
        self.dub_path = abspath(dirname(source))
        self.dub_cfg = dub_cfg
        self.dub_args = dub_args
        self.targets = []
        self.target_names = []
        self.target_paths = []
        self.dpaths = []
        self.dflags = []
        self.libflags = []
        self.libs = []
        self.parse_dub_describe()

    def parse_dub_describe(self):
        with dub_describe(self.dub_source, self.dub_cfg, self.dub_args) as conf:
            for p in conf['packages']:
                if not p['active']:
                    continue
                if p['targetType'].lower().find('library') > -1:
                    target = join(p['path'], p['targetPath'], p['targetFileName'])
                    self.targets.append(target)
                    self.target_names.append(p['targetName'])
                    self.target_paths.append(join(p['path'], p['targetPath']))
                    self.libs += p['libs']
                    self.dpaths += [join(p['path'], x) for x in p['importPaths']]

def dub_emitter(target, source, env):
    dub_desc = env['DUBDESC']
    return dub_desc.targets, source

def dub_build(target, source, env):
    dir_stack = getcwd()
    chdir(env['DUBDESC'].dub_path)
    cmd_line = ['dub']
    if env['DUBCONFIG']:
        cmd_line += ['-c', env['DUBCONFIG']]
    cmd_line += env['DUBARGS']
    system(' '.join(cmd_line))
    chdir(dir_stack)


def DubPackage(env, dub_pkg, **kwargs):
    abs_path = join(env.Dir('#').abspath, dub_pkg)
    dub_cfg = kwargs.pop('DUBCONFIG', None)
    dub_args = kwargs.pop('DUBARGS', [])
    dub_desc = DubDescription(abs_path, dub_cfg, dub_args)
    env.AppendUnique(DPATH=dub_desc.dpaths)
    if env.get('LIBPATH'):
        env['LIBPATH'] += dub_desc.target_paths
    else:
        env['LIBPATH'] = dub_desc.target_paths
    if env.get('LIBS'):
        env.AppendUnique(LIBS=dub_desc.target_names + dub_desc.libs)
    else:
        env['LIBS'] = dub_desc.target_names + dub_desc.libs
    res = env.Dub(abs_path, DUBDESC=dub_desc, DUBCONFIG=dub_cfg, DUBARGS=dub_args)
    env.Default(res)
    return res


def DMDParseConfigCallBack(env, pkg_config):
    args = pkg_config.split()
    for arg in args:
        if arg.startswith('-L') or arg.startswith('-defaultlib'):
            env.Append(DLINKFLAGS=arg)
        elif arg.startswith('-I'):
            env.Append(DPATH=arg[2:])
        else:
            env.Append(DFLAGS=arg)


def DPkgConfig(env, source):
    env.ParseConfig(source, DMDParseConfigCallBack)


def CheckPKGConfig(context, version):
    context.Message('Checking for pkg-config... ')
    ret = context.TryAction(
        'pkg-config --atleast-pkgconfig-version=%s' % version)[0]
    context.Result(ret)
    return ret


def CheckPKG(context, name):
    context.Message('Checking for %s... ' % name)
    ret = context.TryAction('pkg-config --exists \'%s\'' % name)[0]
    context.Result(ret)
    return ret

def DUnitTest(env, source, use_dunit='3rd-party/dunit/dub.json', unit_threaded='3rd-party/unit-threaded/dub.json', **kwargs):
    test_env = env.Clone()
    if use_dunit:
        test_env.DubPackage(use_dunit)
        libs = kwargs.pop('LIBS', [])
        test_env.AppendUnique(LIBS=libs)
        tests = test_env.DProgram('unit-test', source, **kwargs)
    elif unit_threaded:
        test_env.DubPackage(unit_threaded)
        libs = kwargs.pop('LIBS', [])
        test_env.AppendUnique(LIBS=libs)
        unit_threaded_source = test_env.GenUnitThreadedRunner(source)
        tests = test_env.DProgram('ut-test', [unit_threaded_source] + source, **kwargs)
    test_env.Default(tests)
    tests_alias = test_env.Alias('test', [tests], tests[0].path)
    test_env.Default(tests_alias)
    test_env.AlwaysBuild(tests_alias)
    return tests

def gen_unit_threaded_ut_d_emitter(target, source, env):
    return "ut_main.d", source

def gen_unit_threaded_ut_d(target, source, env):
    template = """
import std.stdio;
import unit_threaded.runner;
int main(string[] args)
{
    return args.runTests!("sync.plugin");
}
    """
    f = open(str(target[0]), "w")
    f.write(template)
    f.close()

def generate(env):
    action = '$DC $DFLAGS $_DINCFLAGS $_DVERFLAGS $_DDEBUGFLAGS -D -Df$TARGET $SOURCE -o-'
    bld = Builder(action=action,
                  suffix='.html',
                  src_suffix='.d',
                  single_source=1)
    env.Append(BUILDERS={'DDoc': bld})
    bld = Builder(action=dub_build, emitter=dub_emitter)
    env.Append(BUILDERS={'Dub': bld})
    bld = Builder(action=gen_unit_threaded_ut_d, emitter=gen_unit_threaded_ut_d_emitter)
    env.Append(BUILDERS={'GenUnitThreadedRunner': bld})
    env.AddMethod(DPkgConfig, "DPkgConfig")
    env.AddMethod(DubPackage, 'DubPackage')
    env.AddMethod(DUnitTest, 'DUnitTest')


def exists(env):
    return True
