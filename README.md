My SCons's site_tools
===

This repository contains my SCons's site_tools module (see the [SCons man page](http://scons.org/doc/HTML/scons-man.html)).

It provide tools (`dhelpers`) and a useful SCons Environment subclass (`DEnv`) to work with the [D language](https://dlang.org/).
